# %%
import pandas as pd
import numpy as np
import re
import glob
from collections import namedtuple
from statistics import mean, stdev
from pathlib import Path
from pptx import Presentation
from pptx.util import Inches

#######################################
##
##    Data Processing
##
#######################################

# Concat CSV files Helper Functions
def search_files_recursive(data_folder) -> set:
    nksd_files = glob.glob(data_folder + '/**/*.nksd', recursive=True)
    return set(nksd_files)

def combine_csv(all_files) -> pd.DataFrame:
    dfs = [pd.read_csv(f, header=[0,1], engine='python', error_bad_lines=True) for f in all_files]
    df = pd.concat(dfs, axis=0)
    df = df[dfs[0].columns] # restore original columns order
    df.to_csv("data//GRR_Combined.csv", index=False, encoding='utf-8-sig')
    return df

# DataFrame.Apply Helper Functions
def assign_mount_col(product) -> str:
    return str(product.split("_")[2])

def assign_lul_col(product) -> str:
    return str(product.split("_")[3])

def assign_track_col(track_meas, regex) -> str:
    track = re.search(regex, track_meas)
    if track.group(1):
        return track.group(1)
    else:
        return 0

def assign_meas_col(track_meas, regex) -> str:
    track = re.search(regex, track_meas)
    if track.group(2):
        return track.group(2)
    else:
        return 0


#######################################
##
##    PPT
##
#######################################

class PPT:  
    def __init__(self, pptx=None, size=[]):
        self.prs = Presentation(pptx)
        if not pptx:
            if size == []:
                self.prs.slide_width = Inches(4)
                self.prs.slide_height = Inches(3)
            else: 
                self.prs.slide_width = Inches(size[0])
                self.prs.slide_height = Inches(size[1])
        
    def new_slide(self):
        title_only_slide_layout = self.prs.slide_layouts[5]
        slide = self.prs.slides.add_slide(title_only_slide_layout)
        shapes = slide.shapes

        shapes.title.txt = 'Adding a Table'

        rows = cols = 2
        left = top = Inches(2.0)
        width = Inches(6.0)
        height = Inches(0.8)

        table = shapes.add_table(rows, cols, left, top, width, height).table

        # write column headings
        table.cell(0, 0).text = 'Foo'
        table.cell(0, 1).text = 'Bar'

        # write body cells
        table.cell(1, 0).text = 'Baz'
        table.cell(1, 1).txt = 'Qux'

    def save(self):
        self.prs.save('grr.pptx')

    def wew(self):
        lyt = self.prs.slide_layouts[1]
        slide = self.prs.slides.add_slide(lyt)
        shapes = slide.shapes

        title = shapes.title
        subtitle = shapes.placeholders[1]
    
        title.text = "WEW"
        subtitle.text = "WEW2T.RawResult_nm"

#######################################
##
##    Main Function
##
#######################################
def summarize_GRR(data, result_name_dict):

    dict_mean_prog = dict()
    dict_std_prog = dict()
    dict_cv_prog = dict()
    dict_v_prog = dict()

    dict_mean_agg = dict()
    dict_std_agg = dict()
    dict_cv_agg = dict()
    dict_v_agg = dict()

    dict_delta = dict()
    dict_delta_T = dict()

    for regex, name in result_name_dict.items():
        # Melt (Stack) DataFrame
        unpivot_vars = data.columns.str.match(regex)
        id_vars = np.delete(data.columns, unpivot_vars)
        melt = data.melt(id_vars=id_vars, var_name='Track_Meas', value_name=regex)
        melt["Track"] = melt.Track_Meas.apply(assign_track_col, args=(regex,))
        melt["MeasurementIter"] = melt.Track_Meas.apply(assign_meas_col, args=(regex,))
        
        # Build Index
        index = ['QMTCell', 'DiskSN', 'Mount', 'LUL', 'Track', 'MeasurementIter']
        melt['Mount'] = melt.Product.apply(assign_mount_col)
        melt['LUL'] = melt.Product.apply(assign_lul_col)
        melt.set_index(index, inplace=True)
        melt[regex] = pd.to_numeric(melt[regex], downcast='float')  #given normal result name
        
        # Build Pivot Table based on Zone
        df = pd.pivot_table(melt, values=regex, index=index, columns='Zone')

        # Calculate Mean / Sigma / CV / Delta Mean in two ways
        # Progressive
        ret_mean_prog=[]
        ret_std_prog = []
        ret_cv_prog = []
        ret_v_prog= []
        ret_delta_prog = []
        mean_prog = [df]
        std_prog = [df]
        v_prog = [df]
        delta_prog = []

        for i in range(len(index) - 1):
            g_mean = mean_prog[-1].groupby(level=index[:-i-1]).mean()
            g_std = std_prog[-1].groupby(level=index[: -i - 1]).std()
            g_var = v_prog[-1].groupby(level=index[:-i-1]).var()
            mean_prog.append(g_mean)
            std_prog.append(g_std)
            ret_mean_prog.append(g_mean.mean().to_frame(name='->'.join(index[: i - 1])).T)
            ret_std_prog.append(g_std.mean().to_frame(name='->'.join(index[:-i-1])).T)
            ret_cv_prog.append((g_std.mean() / g_mean.mean()).to_frame(name='->'.join(index[:-i-1])).T)
            ret_v_prog.append(g_var.mean().to_frame(name='->'.join(index[:-i-1])).T)
        # Aggregation
        ret_mean_agg = []
        ret_std_agg = []
        ret_cv_agg = []
        ret_v_agg = []
        level_delta_map = {}
        for i in range(len(index) - 1):
            #delta mean
            grouped_mean = df.groupby(level=df.index.names[:-i] if i != 0 else df.index.names).mean()
            next_grouped_mean = df.groupby(level=df.index.names[:-i-1]).mean()
            temp = grouped_mean.copy()
            if type(next_grouped_mean.index) == pd.MultiIndex:
                temp.update(next_grouped_mean)
                #temp_list.append(temp)
                #delta_list.append(grouped_mean - temp)
                level_delta_map[df.index.names[-i-1]] = grouped_mean - temp
            else:
                # pandas does not support update multiindex df with single index df
                temp_reidx = temp.reset_index(level=1)
                temp_reidx.update(next_grouped_mean)
                temp_reidx = temp_reidx.set_index(df.index.names[-i - 1], append=True)
                #temp_list.append(temp_reidx)
                #delta_list.append(grouped_mean - temp_reidx)
                level_delta_map[df.index.names[-i - 1]] = grouped_mean - temp_reidx
                
            g_mean = df.groupby(level=index[:-i-1]).mean()
            g_std = df.groupby(level=index[: -i - 1]).std()
            g_var = df.groupby(level=index[: -i - 1]).var()

            ret_mean_agg.append(g_mean.mean().to_frame(name=index[-i-1]).T)
            ret_std_agg.append(g_std.mean().to_frame(name=index[-i-1]).T)
            ret_cv_agg.append((g_std.mean() / g_mean.mean()).to_frame(name=index[-i-1]).T)
            ret_v_agg.append(g_var.mean().to_frame(name=index[-i-1]).T)
        # Result DataFrame
        dict_cv_prog[name] = pd.concat(ret_cv_prog, axis=0)
        dict_std_prog[name] = pd.concat(ret_std_prog, axis=0)
        dict_mean_prog[name] = pd.concat(ret_mean_prog, axis=0)
        dict_v_prog[name] = pd.concat(ret_v_prog, axis=0)
        dict_cv_agg[name] = pd.concat(ret_cv_agg, axis=0)
        dict_std_agg[name] = pd.concat(ret_std_agg, axis=0)
        dict_mean_agg[name] = pd.concat(ret_mean_agg, axis=0)
        dict_v_agg[name] = pd.concat(ret_v_agg, axis=0)
        dict_delta[name] = level_delta_map
    
    prog = Method(pd.concat(dict_mean_prog, axis=1), pd.concat(dict_std_prog, axis=1), pd.concat(dict_cv_prog, axis=1), pd.concat(dict_v_prog, axis=1))
    agg = Method(pd.concat(dict_mean_agg, axis=1), pd.concat(dict_std_agg, axis=1), pd.concat(dict_cv_agg, axis=1), pd.concat(dict_v_agg, axis=1))
    for name, deltas in dict_delta.items():
        for level, data in deltas.items():
            if level not in dict_delta_T:
                dict_delta_T[level] = dict()
            dict_delta_T[level][name] = data
    return Summary(agg, prog, dict_delta_T)


data_folder = r"C:\Users\1000262815\Desktop\GRR\full"
#data_folder = r'C:\Users\1000262815\Desktop\GRR\slim'
output_folder = "output//"
#data_folder = r"\\csf-op-gench02.wdc.com\ATE_Public\Xuliang\Newton\GRR\MHT006\\"

regex = [
    "WEW2T([-+]\d)?_(\d)?.RawResult_nm",
    "WEW2T([-+]\d)?_(\d)?.RawMWWNMResult",
    "SERSqz_R0_Const([-+]\d)?_(\d)?.SERSqz0",
    "SERSqz_R0_Const([-+]\d)?_(\d)?.SERSqz1",
    'Overwrite([-+]\d)?_(\d)?.RawResult',
    'Amplitude([-+]\d)?_(\d)?.RawLFResult',
    'Amplitude([-+]\d)?_(\d)?.RawHFResult',
    'Amplitude([-+]\d)?_(\d)?.RawResolutionResult',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_Total',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_1T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_2T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_3T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_4T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_5T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_6T',
]

# Concat csv files
all_files = search_files_recursive(data_folder)
data = combine_csv(all_files)
result_name_dict = {}
for rx in regex:
    for col_pair in data.columns:
        if re.compile(rx).match(col_pair[0]):
            if col_pair[0] not in result_name_dict:
                result_name_dict[rx] = col_pair[1]
data.columns = data.columns.droplevel(1) #ignore second row header

# Summarize
Summary = namedtuple('Summary', 'agg prog delta')
Method = namedtuple('Method', 'mean std cv var')
summary = summarize_GRR(data, result_name_dict)

output_file = 'grr.xlsx'
with pd.ExcelWriter(output_file) as writer:
    # summary.agg.mean.to_excel(writer, sheet_name='agg_mean')
    # summary.agg.std.to_excel(writer, sheet_name='agg_std')
    # summary.agg.cv.to_excel(writer, sheet_name='agg_cv')
    # summary.prog.mean.to_excel(writer, sheet_name='prog_mean')
    # summary.prog.std.to_excel(writer, sheet_name='prog_std')
    # summary.prog.cv.to_excel(writer, sheet_name='prog_cv')
    for method_name, method in summary._asdict().items():
        if type(method) is not dict:
            for stats_name, stats in method._asdict().items():
                stats.to_excel(writer, sheet_name='{}_{}'.format(method_name, stats_name))
        else:
            for level, df_dict in method.items():    
                pd.concat(df_dict, axis=1).to_excel(writer, sheet_name=level+'_delta_mean')

# %%
