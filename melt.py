# %%
import pandas as pd
import numpy as np
import re
import glob
from collections import namedtuple
from statistics import mean, stdev
from pathlib import Path
from pptx import Presentation
from pptx.util import Inches


# Concat CSV files Helper Functions
def search_files_recursive(data_folder) -> set:
    nksd_files = glob.glob(data_folder + '/**/*.nKSD', recursive=True)
    return set(nksd_files)

def combine_csv(all_files) -> pd.DataFrame:
    dfs = [pd.read_csv(f, header=[0,1], engine='python', error_bad_lines=True) for f in all_files]
    df = pd.concat(dfs, axis=0)
    df = df[dfs[0].columns] # restore original columns order
    df.to_csv("data//GRR_Combined.csv", index=False, encoding='utf-8-sig')
    return df

# DataFrame.Apply Helper Functions
def assign_mount_col(product) -> str:
    return str(product.split("_")[2])

def assign_lul_col(product) -> str:
    return str(product.split("_")[3])

def assign_track_col(track_meas, regex) -> str:
    track = re.search(regex, track_meas)
    if track.group(1):
        return track.group(1)
    else:
        return 0

def assign_meas_col(track_meas, regex) -> str:
    track = re.search(regex, track_meas)
    if track.group(2):
        return track.group(2)
    else:
        return 0


data_folder = r"C:\Users\1000262815\Desktop\GRR\full"
#data_folder = r'C:\Users\1000262815\Desktop\GRR\slim'
output_folder = "output//"
#data_folder = r"\\csf-op-gench02.wdc.com\ATE_Public\Xuliang\Newton\GRR\MHT006\\"

regex = [
    "WEW2T([-+]\d)?_(\d)?.RawResult_nm",
    "WEW2T([-+]\d)?_(\d)?.RawMWWNMResult",
    "SERSqz_R0_Const([-+]\d)?_(\d)?.SERSqz0",
    "SERSqz_R0_Const([-+]\d)?_(\d)?.SERSqz1",
    'Overwrite([-+]\d)?_(\d)?.RawResult',
    'Amplitude([-+]\d)?_(\d)?.RawLFResult',
    'Amplitude([-+]\d)?_(\d)?.RawHFResult',
    'Amplitude([-+]\d)?_(\d)?.RawResolutionResult',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_Total',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_1T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_2T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_3T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_4T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_5T',
    'WSSNR([-+]\d)?_(\d)?.WSSNR_6T',
]
index = ['Zone', 'QMTCell', 'DiskSN', 'Mount', 'LUL', 'Track', 'MeasurementIter']


# Concat csv files
all_files = search_files_recursive(data_folder)
data = combine_csv(all_files)
result_name_dict = {}
for rx in regex:
    for col_pair in data.columns:
        if re.compile(rx).match(col_pair[0]):
            if col_pair[0] not in result_name_dict:
                result_name_dict[rx] = col_pair[1]
data.columns = data.columns.droplevel(1)  #ignore second row header

melt_dfs = []
for regex, name in result_name_dict.items():
    # Melt (Stack) DataFrame
    unpivot_vars = data.columns.str.match(regex)
    id_vars = np.delete(data.columns, unpivot_vars)
    melt = data.melt(id_vars=id_vars, var_name='Track_Meas', value_name=name)
    melt["Track"] = melt.Track_Meas.apply(assign_track_col, args=(regex,))
    melt["MeasurementIter"] = melt.Track_Meas.apply(assign_meas_col, args=(regex,))
    
    # Build Index
    melt['Mount'] = melt.Product.apply(assign_mount_col)
    melt['LUL'] = melt.Product.apply(assign_lul_col)
    melt.set_index(index, inplace=True)
    #melt[name] = pd.to_numeric(melt[name], downcast='float')  #given normal result name

    melt_dfs.append(pd.DataFrame(melt, columns=[name]))

df = pd.concat(melt_dfs, axis=1)
df.to_csv("output//Indexed_GRR_Raw.csv")
#df.reset_index().to_csv("output//GRR_Raw.csv")

    
# %%
